﻿using System;
namespace Mic.Homework.Shapes.Shape.Line
{
    class Line : Shape
    {
        private byte _height;
        public sealed override byte Height
        {
            get => _height;
            set
            {
                _height = value;
                _width = 0;
            }
        }

        private byte _width;
        public sealed override byte Width
        {
            get => _width;
            set
            {
                _width = value;
                _height = 0;
            }
        }

        public sealed override double Area()
        {
            return 0;
        }

        public bool IsVertical => _height != 0;

        public override void Draw()
        {
            if (IsVertical)
            {
                for (int i = 0; i < _height; i++)
                    Console.WriteLine("|");
            }
            else
            {
                Console.Write(new string('-', _width));
            }
        }
    }

}
