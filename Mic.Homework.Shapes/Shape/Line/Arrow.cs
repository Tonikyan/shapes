﻿using System;

namespace Mic.Homework.Shapes.Shape.Line
{
    class Arrow : Line
    {
        public override void Draw()
        {
            base.Draw();
            if (IsVertical)
                Console.WriteLine("V");
            else
                Console.Write(">");
        }
    }
}
