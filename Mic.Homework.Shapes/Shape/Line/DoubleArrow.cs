﻿using System;
namespace Mic.Homework.Shapes.Shape.Line
{
    class DoubleArrow : Line
    {
        public override void Draw()
        {
            if (IsVertical)
                Console.WriteLine("^");
            else
                Console.Write("<");
            base.Draw();
            if (IsVertical)
                Console.WriteLine("v");
            else
                Console.Write(">");
        }
    }
}
