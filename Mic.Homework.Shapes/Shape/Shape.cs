﻿using System;
namespace Mic.Homework.Shapes.Shape
{
    abstract public class Shape
    {
        public virtual byte Height { get; set; }

        public virtual byte Width { get; set; }

        public abstract Double Area();

        public virtual void Draw() { }
    }
}
