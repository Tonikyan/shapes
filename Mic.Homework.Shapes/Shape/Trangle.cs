﻿using System;
namespace Mic.Homework.Shapes.Shape
{
    class Trangle : Shape
    {
        public override void Draw()
        {
            for (int i = 0; i < Height; i++)
            {
                if (i == Height - 1)
                    for (int j = 0; j < Height; j++)
                        Console.Write("* ");
                for (int j = 0; j < Height - (i + 1); j++)
                    Console.Write(" ");
                for (int j = 0; j < 2 * i + 1; j++)
                {
                    if (i != Height - 1)
                        if (j == 0 || j == 2 * i)
                        {

                            Console.Write("*");
                        }
                        else
                            Console.Write(" ");
                }
                Console.WriteLine();
            }
        }

        public override Double Area()
        {
            double S = Math.Sqrt(3) * Height / 4;
            return S;
        }
    }
}
