﻿using System;
namespace Mic.Homework.Shapes.Shape
{
    class Square : Rectangle
    {
        private byte _height;
        public sealed override byte Height
        { 
        get => _height;
        set
            {
                _height = value;
                _width = value;
            }
        }
        private byte _width;
        public sealed override byte Width 
        {
            get => _width;
            set
            {
                _width = value;
                _height = value;
            }
        }
    }
}
