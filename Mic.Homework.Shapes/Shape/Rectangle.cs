﻿using System;
namespace Mic.Homework.Shapes.Shape
{
    class Rectangle : Shape
    {
        public override void Draw()
        {
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    if (i == 0 || i == Width - 1)
                        Console.Write("* ");
                    else
                    {
                        if (j == 0 || j == Height - 1)
                            Console.Write("* ");
                        else
                            Console.Write("  ");
                    }
                }
                Console.WriteLine();
            }
        }

        public override Double Area()
        {
            return Width * Height;
        }
    }
}
