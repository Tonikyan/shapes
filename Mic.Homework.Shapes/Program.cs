﻿using System;
using Mic.Homework.Shapes.Shape;
using Mic.Homework.Shapes.Shape.Line;


namespace Mic.Homework.Shapes
{
    class MainClass
    {
        public static void Main(string[] arg)
        {
            var l = new Line()
            {
                Width = 10,
                Height = 20
            };
            l.Draw();
            var a = new Arrow()
            {
                Width = 10,
                Height = 20
            };
            a.Draw();
            var r = new Rectangle()
            {
                Height = 5,
                Width = 15
            };
            r.Draw();
            var da = new DoubleArrow()
            {
                Width = 15,
                Height = 5
            };
            da.Draw();
            var s = new Square()
            {
                Height = 5
            };
            s.Draw();
            var tri = new Trangle()
            {
                Width = 15,
                Height = (byte)Math.Sqrt(3)
            };
            tri.Draw();
        }
    }
}
